from django.db import models
from datetime import datetime as dt
from datetime import timedelta
# Create your models here.

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from django.utils.translation import ugettext as _

#class Activiy:

def time_extra():
	return dt.today()+timedelta(days=1)

class Subscriptor(models.Model):
	"""
	Subscribe to the activity
	"""
	activity=models.ForeignKey(Activiy,
		on_delete=models.CASCADE,
		related_name="subscriptors",#plural
		related_query_name="subscriptor",		
		)
	#{Project, Team, User}
	limit=models.Q(
		app_label='project',
		model='project') | models.Q(
		app_label='team',
		model='team')|models.Q(
		app_label='auth',
		model='user')
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_('content page'),
        limit_choices_to=limit,
        null=True,
        blank=True,
    )
    object_id = models.PositiveIntegerField(
        verbose_name=_('related object'),
        null=True,
    )
    content_object = generic.GenericForeignKey(
    	'content_type', 
    	'object_id')		

class RememberRule(models.Model):
	"""
	A special rule to send messages before activiy
	"""
	CHOICE_TYPE_TIME=[
	('mn',_('minutes')),
	('hr',_('hour')),
	('da',_('day'))]

	CHOICE_MODE=[
	('bf',_('before')),
	('ev',_('every')),
	('fx',_('fixed'))]

	subscriptor=models.ForeignKey(Subscriptor,
		on_delete=models.CASCADE,
		related_name="remember_rules",#plural
		related_query_name="remember_rule",		
		)
	period=models.PositiveIntegerField(default=1)
	type_time=models.CharField(
		max_length=2,
		,choices=CHOICE_TYPE_TIME)
	mode=models.CharField(
		max_length=2,choices=CHOICE_MODE)
	date_ref=models.DateField(default=time_extra)

class AnnonSubscriptor(models.Model):
	"""
	An annonimous subscriptor with a counter with the number of
	time that annon person suscribe to an evento on this city
	"""
	activity=models.ForeignKey(Activiy,
		on_delete=models.CASCADE,
		related_name="subscriptors",#plural
		related_query_name="subscriptor",		
		)
	name=models.CharField(max_length=30)
	email=models.EmailField()
	counter=models.PositiveIntegerField()
	#Rule-> 1 week before and 1 day before
