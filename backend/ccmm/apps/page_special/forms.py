from .models import Pagina
from django.forms import ModelForm
from dal import autocomplete

class PageForm(ModelForm):
    class Meta:
        model = Pagina
        fields = [ 'titulo', 'imagen', 'resumen', 'cuerpo', 'clasificacion' ]
        widgets = {
            'clasificacion' : autocomplete.ModelSelect2( url = 'clasificacion-autocomplete')
        }
