from django.contrib import admin
from .models import Page, Clasification
# Register your models here.



class PageAdmin(admin.ModelAdmin):
	list_display = ("clasification","title","pub_date")
	class Media:
		js = ('ckeditor/ckeditor/ckeditor.js')


admin.site.register(Page,PageAdmin)
admin.site.register(Clasification)