from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from . import managers
#ckeditor model
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
#slug
from autoslug import AutoSlugField
import itertools
from django.utils import timezone
from datetime import datetime,time
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django.urls import reverse


def get_upload_pagina_file_name(instance, filename):
    return "image/pages/%s/%s" %(instance.slug_titulo, filename)

class Clasification(models.Model):
    type_page = models.CharField(
        max_length=20,
        unique=True,
        verbose_name=_("Type page"))
    slug_type = AutoSlugField(
        blank=True,
        populate_from='type_page', 
        editable=True,
        verbose_name=_("Page's Slug"))
    description = models.TextField(
        blank=True,
        verbose_name=_("Description"))
    pub_date = models.DateTimeField(
        auto_now=True,
        verbose_name=_("Publication Date"))
    content_type = models.ForeignKey(ContentType, 
        on_delete=models.CASCADE, 
        null=True, 
        blank=True, 
        editable=False)
    object_id = models.PositiveIntegerField(
        null=True, 
        blank=True, 
        editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def get_absolute_url(self):
        return reverse('clasification:post', args=[self.slug_tipo,])

    @property
    def Clasification(self):
        return self.tipo

    class Meta:
        app_label = "page_special"
        verbose_name = _("Clasification")
        verbose_name_plural = _("Clasifications")
        ordering = ("type_page","pub_date")

    def __str__(self):
        return self.tipo

class Page(models.Model):
    title = models.CharField(max_length=100,verbose_name=_("Title"))
    slug_title =  AutoSlugField(unique=True,
        blank=True,
        populate_from='title', 
        editable=True,
        verbose_name=_("Title's Slug"))
    image = models.ImageField(
        upload_to=get_upload_pagina_file_name, 
        blank=True,
        verbose_name=_("Image"))
    pub_date = models.DateTimeField(auto_now=True,verbose_name=_("Publication date"))
    resume = models.TextField(default="Escribe un resumen",verbose_name=_("Resume"))
    body = RichTextUploadingField(help_text=_("Write the content"),verbose_name=_("Content body"))
    clasification = models.ForeignKey(Clasification,blank=True,null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')


    def get_absolute_url(self):
        return reverse('page_edit', kwargs={'pk':self.pk})

    @property
    def Page(self):
        return self.title

    class Meta:
        app_label = "page_special"
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ("clasification","pub_date")

    def __str__(self):
        return self.title

