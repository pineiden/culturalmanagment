from django.shortcuts import get_object_or_404, render
from .models import Page, Clasification
from django.conf import settings
#autcomplete view
#https://django-autocomplete-light.readthedocs.org/en/master/tutorial.html

from dal import autocomplete

class ClasificationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return  Clasificacion.objects.none()

        qs = Clasificacion.objects.all()

        if self.q:
            qs = qs.filter( tipo__istartswith= self.q )

        return qs

# Create your views here.

def see_page ( request, this_slug_title ):
    pagina = get_object_or_404 ( Pagina, slug_titulo=this_slug_title )
    static_media_url = settings.MEDIA_URL
    context = { 'slug_title': this_slug_titulo, 'page': pagina, 'static_media_url': static_media_url }
    return render ( request, 'pagina.html', context )


def see_clasification ( request, this_slug_type ):
    c_class = get_object_or_404 ( Clasificacion, slug_type=this_slug_type )
    pages = Pagina.objects.filter ( clasificacion=clase.id )
    context = { 'slug_tipo': this_slug_type, 'clase': c_class, 'pages': pages }
    return render ( request, 'tipo.html', context )


from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import PageForm

class PageList ( LoginRequiredMixin,ListView ):
    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    template_name = "pagina/pagina_list.html"
    model = Page


class PageCreate ( LoginRequiredMixin,CreateView ):
    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    form_class = PaginaForm
    template_name = "pagina/pagina_form.html"
    success_url = reverse_lazy ( 'page_list' )


class PageUpdate ( LoginRequiredMixin,UpdateView ):
    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    form_class = PageForm
    template_name = "pagina/pagina_form.html"
    success_url = reverse_lazy ( 'page_list' )



class PageDelete ( LoginRequiredMixin,DeleteView ):
    login_url = '/user/login/'
    redirect_field_name = 'redirect_to'
    model = Page
    success_url = reverse_lazy ( 'page_list' )
