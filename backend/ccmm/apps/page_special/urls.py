from django.conf.urls import url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

urlpatterns = [
    url(r'^pagina/(?P<this_slug_titulo>[\w-]+)/$', views.ver_pagina, name='ver_pagina'),
    url(r'^clasificacion/(?P<this_slug_tipo>[\w-]+)/$', views.ver_clasificacion),
    url(r'^listar/paginas$',views.PaginaList.as_view(), name='pagina_list'),
    url(r'^agregar/pagina$', views.PaginaCreate.as_view(), name='pagina_new'),
    url(r'^editar/pagina/(?P<pk>\d+)$', views.PaginaUpdate.as_view(), name='pagina_edit'),
    url(r'^borrar/pagina/(?P<pk>\d+)$', views.PaginaDelete.as_view(), name='pagina_delete'),
    url(r'^clasificacion-autocomplete/$',views.ClasificacionAutocomplete.as_view( create_field = 'tipo' ), name='clasificacion-autocomplete')
]
