from django.contrib import admin

from .models import Team, TypeRole, TeamRole, TeamMember
# Register your models here.



admin.site.register(Team)
admin.site.register(TypeRole)

admin.site.register(TeamRole)
admin.site.register(TeamMember)