from django.db import models

#Import from apps
from apps.ccmm_profile.models import CultureManagmentUser
from apps.organization.models import Organization
# Create your models here.

# Multilanguage

from django.utils.translation import ugettext_lazy as _


class Team(models.Model):
	"""
	The Team is an element created to work in a project
	and belongs to an Organization
	"""
	name=models.CharField(
		max_length=30,
		default=_("Work"),
		verbose_name=_("Name"))
	description=models.TextField(
		verbose_name=_("Description"))
	organization=models.ForeignKey(
		Organization,
		verbose_name=_("Organization"))

	def __str__(self):
		return self.name 

	class Meta:
		verbose_name=_("Team")
		verbose_name_plural=_("Teams")

class TypeRole(models.Model):
	"""
	Define roles and permissions to an user
	"""
	rolename=models.CharField(
		max_length=50, 
		default=_("Organizator"))
	description=models.TextField(
		verbose_name=_("Description"))
	edit_docs=models.BooleanField(
		default=False,
		verbose_name=_("Edit Documents"))
	edit_album=models.BooleanField(
		default=False,
		verbose_name=_("Edit Albums"))
	edit_tasks=models.BooleanField(
		default=False,
		verbose_name=_("Edit Tasks"))
	edit_members=models.BooleanField(
		default=False,
		verbose_name=_("Edit Members"))
	
	def __str__(self):
		return self.rolename

	class Meta:
		verbose_name=_("Type of Role")
		verbose_name_plural=_("Type of Roles")

class TeamRole(models.Model):
	"""
	Define the role of a Team
	"""
	name=models.CharField(max_length=50, default="NoRole")
	typerole=models.ForeignKey(TypeRole)
	team=models.ForeignKey(Team)
	class Meta:
		verbose_name=_("Team Role")
		verbose_name_plural=_("Team Roles")

	def __str__(self):
		return "%s on %s" %(
			self.typerole.name, 
			self.team.name)

class TeamMember(models.Model):
	"""
	Associate an user with a Team, and assign
	the Team Role
	"""
	team=models.ForeignKey(Team) 
	user=models.ForeignKey(CultureManagmentUser)
	role=models.ForeignKey(TeamRole)
	class Meta:
		verbose_name=_("Team Member")
		verbose_name_plural=_("Team Members")

	def __str__(self):
		return "%s:%s:%s" %(
			self.team,
			self.user,
			self.role
			)