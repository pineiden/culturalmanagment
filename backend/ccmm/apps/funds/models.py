from django.db import models
from apps.organization.models import Organization
# Create your models here.
from scripts.currency import get_currencies_list
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

class Fund(models.Model):
	"""
	Information about funds whit what the projects are
	working
	I
	"""
	CHOICE_CURRENCY=get_currencies_list()
	organization=models.ForeignKey(Organization)
	call_of_competition=models.CharField(
		max_length=200)
	date_call=models.DateField(
		default=timezone.now)
	date_start=models.DateField(
		default=timezone.now)
	amount_call=models.IntegerField(
		default=0)
	assignation=models.IntegerField(
		default=0)
	currency=models.CharField(
		max_length=3,
		default="CLP",
		choices=CHOICE_CURRENCY)
	class Meta:
		verbose_name=_("Fund")
		verbose_name_plural=_("Funds")

	def __str__():
		return "%s : %s" % (
			self.call_of_competition, self.date_call)
