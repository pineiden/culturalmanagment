from django.db import models
from autoslug import AutoSlugField


#Content types
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation

# L10N
from django.utils.translation import ugettext_lazy as _

# import from apps

from apps.localization.models import Location
from apps.gallery.models import Album

# Create your models here.

class Place(models.Model):
	"""
	Is a physical place related to a territory
	"""
	name=models.CharField(max_length=100)
	slug=AutoSlugField(blank=True,populate_from='name', editable=True)
	description=models.TextField(blank=True)
	localization=models.ForeignKey(Location,
		on_delete=models.CASCADE,
		related_name="locations",#plural
		related_query_name="location",		
		)
	url=models.URLField(max_length=300)
	wiki=models.URLField(max_length=300)
	content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
	object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
	content_object = GenericForeignKey('content_type', 'object_id')

	class Meta:
		verbose_name=_("Place")
		verbose_name_plural=_("Places")

class PlaceAlbum(models.Model):
	"""
	The album related to the territory
	"""
	CHOICE_PRIORITY=[(i,i-25) for i in range(51)]# send hardcoded value to settings
	place=models.ForeignKey(Place,
		on_delete=models.CASCADE,
		related_name="place_album",#plural
		related_query_name="place_album",		
		)
	album=models.ForeignKey(Album,
		on_delete=models.CASCADE,
		related_name="place_album",#plural
		related_query_name="place_album",		
		)

	principal=models.BooleanField(default=False)
	position=models.IntegerField(choices=CHOICE_PRIORITY)
	content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
	object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
	content_object = GenericForeignKey('content_type', 'object_id')

	class Meta:
		verbose_name=_("Place's Album")
		verbose_name_plural=_("Place's Albums")
