from django.contrib import admin

from apps.place.models import Place, PlaceAlbum
# Register your models here.

admin.site.register(PlaceAlbum)
admin.site.register(Place)