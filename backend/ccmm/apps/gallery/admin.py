from django.contrib import admin
from apps.gallery.models import Album, Photo, AlbumLabel, PhotoLabel

# Register your models here.


admin.site.register(Album)
admin.site.register(Photo)
admin.site.register(AlbumLabel)
admin.site.register(PhotoLabel)
