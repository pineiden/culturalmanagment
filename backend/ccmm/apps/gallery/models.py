from django.db import models
# L10N
from django.utils.translation import ugettext_lazy as _

#
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# class LogSearchPropietadad(Propiedad):
from imagekit.models import ImageSpecField
from imagekit.processors import SmartResize

# Create your models here.
class Album(models.Model):
	name=models.CharField(max_length=60)
	description=RichTextUploadingField(help_text=_("Write a description for this album"))
	date=models.DateTimeField(
		auto_now_add=True, 
		)
	date_modified=models.DateTimeField(
		auto_now=True, 
		)
	class Meta:
		verbose_name=_("Album")
		verbose_name_plural=_("Albums")

class AlbumLabel(models.Model):
	label=models.CharField(max_length=20)
	album=models.ForeignKey(Album, related_name="album_label")

	class Meta:
		verbose_name=_("Album label")
		verbose_name_plural=_("Albums label")


class Photo(models.Model):
	CHOICE_PRIORITY=[(i,i-25) for i in range(51)]# send hardcoded value to settings
	image = models.ImageField(upload_to='gallery/image')
	middle_image = ImageSpecField(
		source='imagen',
		processors=[SmartResize(800, 600)],
		format='JPEG',
		options={'quality': 60})
	thumbnail=ImageSpecField(
		source='imagen',
		processors=[SmartResize(200, 180)],
		format='JPEG',
		options={'quality': 60})
	name=models.CharField(max_length=20)
	principal=models.BooleanField(default=False)
	priority=models.IntegerField(choices=CHOICE_PRIORITY)
	class Meta:
		verbose_name=_("Photo")
		verbose_name_plural=_("Photos")


class PhotoLabel(models.Model):
	label=models.CharField(max_length=20)
	album=models.ForeignKey(Photo, related_name="album_label")

	class Meta:
		verbose_name=_("Photo label")
		verbose_name_plural=_("Photos label")