from django.db import models
from datetime import datetime
# Create your models here.
from django.utils.translation import ugettext as _

from apps.project.models import Project
from apps.gallery.models import Album
from apps.place.models import Place

class TypeOfActivity(models.Model):
	"""
	Define the types of activities that you will
	do
	"""
	name=models.CharField(max_length=30)
	description=models.TextField()


class Activity(models.Model):
	"""
	Define the main data for this activity,
	related objects, etc

	"""
	name=models.CharField(max_length=100)
	poster=models.ForeignKey(Album,
		on_delete=models.CASCADE,
		related_name="activities",#plural
		related_query_name="activity",
		)
	project=models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="activities",#plural
		related_query_name="activity",
		)
	alone=models.BooleanField(default=True)
	type_activity=models.ForeignKey(TypeOfActivity,
		on_delete=models.CASCADE,
		related_name="activities",#plural
		related_query_name="activity",
		)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Activity")
		verbose_name_plural=_("Activities")

class SetOfActivities():
	"""
	Create the particular information for the
	activity or the full program of a group
	"""
	activity=models.ForeignKey(
		Activity,
		on_delete=models.CASCADE,
		related_name="setsofactivities",#plural
		related_query_name="setofactivities",
		)
	place=models.ForeignKey(Place,
		on_delete=models.CASCADE,
		related_name="setsofactivities",#plural
		related_query_name="setofactivities",
		)
	name=models.CharField(max_length=100)
	description=models.TextField()
	time_start=models.DateTimeField(default=datetime.now)
	time_end=models.DateTimeField(default=datetime.now)

	def __str__(self):
		return "%s: %s" %(self.name, self description)


	class Meta:
		verbose_name=_("Set of Activities")
		verbose_name_plural=_("Sets of Activities")

