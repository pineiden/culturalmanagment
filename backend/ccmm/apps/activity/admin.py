from django.contrib import admin

# Register your models here.
from apps.activity.models import TypeOfActivity, Activity, SetOfActivities


admin.site.register(TypeOfActivity)
admin.site.register(Activity)
admin.site.register(SetOfActivities)