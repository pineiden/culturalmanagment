"""mjd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from password_reset.views import Reset,Recover,RecoverDone,ResetDone
from django.core.urlresolvers import reverse


from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import ugettext_lazy as _


from . import views


urlpatterns = i18n_patterns(
    url(_(r'^list$'),
        views.UsuarioList.as_view(), 
        name='users_list'),
    url(_(r'^add$'), 
        views.UsuarioCreate.as_view(), 
        name='user_new'),
    url(_(r'^register$'), 
        views.UsuarioRegistre.as_view(), 
        name='user_register'),
    url(_(r'^edit/(?P<pk>\d+)$'), 
        views.UsuarioUpdate.as_view(), 
        name='user_edit'),
    url(_(r'^delete/usuario/(?P<pk>\d+)$'), 
        views.UsuarioDelete.as_view(), 
        name='user_delete'),
    url(_(r'^profile/(?P<pk>\d+)$'),
        views.MyProfileView.as_view(), 
        name='user_profile_id'),
    url(_(r'^profile/id/(?P<pk>\d+)/$'),
        views.UsuarioDetail.as_view(), 
        name='user_profile_id'),
    url(_(r'^perfil/username/(?P<slug>[\w+.@+-]+)/$'),
        views.UsuarioDetail.as_view(), 
        name='user_profile_slug'),
    url(_(r'^login/$'),
        views.LoginView.as_view(), 
        name='user_login'),
    url(_(r'^logout/$'), 
        views.LogoutView.as_view(),
        name='user_logout'),
    url(_('^', 
        include('django.contrib.auth.urls')),
    url(_(r'^password/recover/$'),
        auth_views.password_reset,
        {
        'template_name':'usuario/registration/password_reset_form.html',
        'email_template_name':'usuario/registration/password_reset_email.html',
        'subject_template_name':'usuario/registration/password_reset_subject.txt'},
        name='password_reset'),
    url(_(r'^password/recover/done$'),
        auth_views.password_reset_done,
        {
        'template_name':'usuario/registration/password_reset_done.html'},
        name='password_reset_done'),
    url(_(r'^password/change/$'),
        auth_views.password_change,{
        'template_name':'usuario/registration/password_change_form.html'},
        name='password_change'),
    url(_(r'^password/change/done$'),
        auth_views.password_change_done,
        {
        'template_name':'usuario/registration/password_change_done.html'},
        name='password_change_done'),    #url(r'^usuario/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',       auth_views.password_reset_confirm,{'post_reset_redirect' : '/user/password/hecho/'}),
    #url(r'^usuario/password/hecho/$', auth_views.password_reset_complete),
)