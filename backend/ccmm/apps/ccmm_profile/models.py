from django.db import models
from django.contrib.auth.models import User, UserManager
from django.utils.translation import ugettext_lazy as _

from .managers import ProfileManager

from .groups import LIST_GROUPS

# Create your models here.
class CultureManagmentUser(models.Model):
    USER_CHOICES = LIST_GROUPS
    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE, 
        default = 1)
    nickname=models.CharField(
        max_length=20,blank=True)
    name = models.CharField(
        max_length=50,blank=True)
    surname_father = models.CharField(
        max_length=50,blank=True)
    surname_mother = models.CharField(
        max_length=50,blank=True)
    phone = models.IntegerField(
        blank=True)
    imagen = models.ImageField(
        upload_to='user_image', 
        blank=True)
    group = models.CharField(
        max_length=50,
        choices=USER_CHOICES, 
        blank=True)

    objects = ProfileManager()

    @property
    def username(self):
        return "%s %s" %(self.nombre,self.apellido)

    class Meta:
        app_label= 'ccmm_profile'
        verbose_name=_("Culture Manager User")
        verbose_name_plural =_("Culture Manager Users")
        ordering = ("user", )

    def __str__(self):
        return self.username()


class ProfileUserCulturaManagment(models.Model):
    # Relations
    user = models.OneToOneField(
        #settings.AUTH_USER_MODEL
        CultureManagmentUser,
        related_name="profile",
        verbose_name=_("user")
        )
    # Attributes - Mandatory
    interaction = models.PositiveIntegerField(
        default=0,
        verbose_name=_("interaction")
        )
    # Attributes - Optional
    # Object Manager
    objects = ProfileManager()

    # Custom Properties
    def username(self):
        return self.user.user.name

    # Methods

    # Meta and String
    class Meta:
        app_label= 'ccmm_profile'
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")
        ordering = ("user",)

    def __str__(self):
        return self.username()

from django.dispatch import receiver
from django.db.models.signals import post_save

@receiver(post_save, sender=ProfileUserCulturaManagment)
def create_profile_for_new_user(sender, created, instance, **kwargs):
    if created:
        profile = ProfileUserCulturaManagment(user=instance)
        profile.save()