# -*- coding: utf-8 -*-
from django.db import models


class ProfileManager(models.Manager):
    pass

# Administrator
class AdminManager(models.Manager):
    def get_queryset(self):
        return super(AuthorManager, self).get_queryset().filter(role='Admin')
# Culture Manager
class CultureManagerManager(models.Manager):
    def get_queryset(self):
        return super(AuthorManager, self).get_queryset().filter(role='Culture Manager')
# Asistant Manager
class AsistantManager(models.Manager):
    def get_queryset(self):
        return super(AuthorManager, self).get_queryset().filter(role='Asistant')
# Normal User
class NormalManager(models.Manager):
    def get_queryset(self):
        return super(AuthorManager, self).get_queryset().filter(role='Normal')