from django.utils.translation import ugettext_lazy as _

LIST_GROUPS=[('admin',_('Administrator')),
	('culture_manager',_('Culture Manager')),
	('assistant_manager',_('Assistant')),
	('normal',_('Normal'))
	]