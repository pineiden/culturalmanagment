from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .models import CultureManagmentUser,ProfileUserCulturaManagment

class UserCCMMInline(admin.StackedInline):
    model = CultureManagmentUser
    can_delete = False
    verbose_name_plural = _("Users Culture Managment")
# Register your models here.

class UserAdmin(BaseUserAdmin):
    inlines = (UserCCMMInline,)

admin.site.unregister(User)
admin.site.register(User,UserAdmin)

class ProfileAdmin(admin.ModelAdmin):

    list_display = ("user", "interaction")

    search_fields = ["user__username"]

admin.site.register(ProfileUserCulturaManagment,ProfileAdmin)