from django.contrib.auth.forms import UserCreationForm
from .models import CultureManagmentUser

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = CultureManagmentUser
        fields =  ('phone','image','group')


class CustomUserRegisterForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = CultureManagmentUser
        fields =  ('phone','image')

        #Modificar save --> grupo: cliente