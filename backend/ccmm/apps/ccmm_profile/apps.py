from django.apps import AppConfig


class CCMM_ProfileConfig(AppConfig):
    name = 'ccmm_profile'
