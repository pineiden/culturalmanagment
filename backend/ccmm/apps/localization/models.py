#from django.db import models
from django.contrib.gis.db import models as models
from django.utils.translation import ugettext as _
# Create your models here.
from apps.localization.validators import file_size


#Content types
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from languages.fields import LanguageField

def flag_size(value):
    """
    Validate flag file size
    """
    return file_size(value, 1024)

class GeoPoliticalLevelName(models.Model):
    """
    A name for a Geopolitical Level (on a territory)
    """
    name=models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        unique=True)
    language=LanguageField(blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')


    def __str__(self):
        return self.name
        
class Country(models.Model):
    """
    Country is  a model to store data about different countries, in that
    way you can filter the different Countries, hence obtain the set of 
    geopolitical level names
    """
    CHOICE_CONTINENT=(
        ('am',_('America')),
        ('eu',_('Europe')),
        ('af',_('Africa')),
        ('as',_('Asia')),
        ('oc',_('Oceania')),
    )
    continent=models.CharField(
        max_length=2,
        choices=CHOICE_CONTINENT,
        verbose_name=_('continent'),
    )
    name=models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        unique=True)
    flag=models.FileField(
        verbose_name=_('Flag'),
        upload_to='localization/countries_flags',
        validators=[flag_size])
 
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        app_label='localization'
        default_related_name='countries'
        verbose_name_plural= 'countries'

class FirstLevelName(models.Model):
    """
    Inside a country there are a next level on the geopolitical order:
    Country > FirstLevelName

    """
    country=models.ForeignKey(Country,
        related_name ="first_level_name")
    level_name=models.ForeignKey(GeoPoliticalLevelName,
        related_name ="first_level_name")
    content_type = models.ForeignKey(ContentType,
        on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

class SecondLevelName(models.Model):
    """
    Inside a FirstLevel Category, in a country, there are a second level on the geopolitical order:
    FirstLevelName>SecondLevelname
    """
    country=models.ForeignKey(Country,
        related_name ="second_level_name")
    level_name=models.ForeignKey(GeoPoliticalLevelName,
        related_name ="second_level_name")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

class ThirdLevelName(models.Model):
    """
    Inside a SecondLevel Category, in a country, there are a third level on the geopolitical order:
    SecondLevelName>ThirdLevelname
    """

    country=models.ForeignKey(Country,
        related_name ="third_level_name")
    level_name=models.ForeignKey(GeoPoliticalLevelName,
        related_name ="third_level_name")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

class FirstLevel(models.Model):
    """
    This model is for storege the list of first level territories on some country

    """

    name=models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        unique=True)
    code=models.CharField(
        max_length=4,
        verbose_name=_('Number'),
        unique=True,
        default=''
        )
    country=models.ForeignKey(
        Country,
        verbose_name=_('First\'s Level Country'))
    geo_political_name=models.ForeignKey(
        FirstLevelName,
        related_name ="first_level"
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        app_label='localization'
        order_with_respect_to = 'code'
        indexes=[
            models.Index(fields=['code', 'name'])
        ]
    

class SecondLevel(models.Model):
    """
    Model to register the nearest city from the place
    """
    name=models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        unique=True)
    code=models.CharField(
        max_length=6,
        verbose_name=_('Number'),
        unique=True,
        default=''
        )
    parent_level=models.ForeignKey(
        FirstLevel,
        verbose_name=_('Second\'s Level First Level'))
    geo_political_name=models.ForeignKey(
        SecondLevelName,
        related_name ="second_level"
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        app_label='localization'
        order_with_respect_to = 'code'
        indexes=[
            models.Index(fields=['code', 'name'])
        ]

class ThirdLevel(models.Model):
    """
    Model to register the superior place in what the location is placed 
    """ 
    name=models.CharField(
        max_length=50,
        verbose_name=_('Name'),
        unique=True)
    code=models.CharField(
        max_length=10,
        verbose_name=_('Number'),
        default='',
        unique=True        
        )
    url=models.URLField(
        verbose_name=_("URL Address"))
    parent_level=models.ForeignKey(
        SecondLevel,
        verbose_name=_('Third\'s Level Second Level'))
    geo_political_name=models.ForeignKey(
        ThirdLevelName,
        related_name ="third_level"
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        app_label='localization'
        order_with_respect_to = 'code'
        indexes=[
            models.Index(fields=['code', 'name'])
        ]
   
from django.contrib.gis.geos import Point
import geocoder

class Location(models.Model):
    """
    Model to register a location, you have to give the main data to search the
    geo point
    """
    street=models.CharField(
        max_length=50,
        verbose_name=_('Street'))
    number=models.IntegerField()
    geopoint=models.PointField()
    third_level=models.ForeignKey(
        ThirdLevel,
        verbose_name=_('Location\'s Third Level'),
        related_name ="location"
        )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,blank=True, null=True)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def save(self, *args, **kwargs):
        country=third_level.parent_level.parent_level.country.name
        first_level=third_level.parent_level.parent_level.name
        second_level=third_level.parent_level.name
        third_level=third_level.name
        g=geocoder.google("%s,%s,%s,%s,%s %s" %(
            country, 
            first_level,
            second_level,
            third_level,
            street,
            number))
        latlon=g.latlng
        point = Point(x=latlon[0], 
            y=latlon[1], 
            z=0, srid=4326)
        geopoint=point
        super(...).save(*args, **kwargs)

    class Meta:
        app_label='localization'
        default_related_name = _('location')
        order_with_respect_to = 'street'
        indexes=[
            models.Index(fields=['street', 'number', 'third_level'])#third_level.geo_political_name.level_name.name
        ]
        verbose_name=_('Location')


