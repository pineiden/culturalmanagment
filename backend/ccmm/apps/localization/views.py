from django.shortcuts import render

# Create your views here.

# Generic Views Class
from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth.mixins import PermissionRequiredMixin

from django.views.generic import View
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DetailView
from django.views.generic import FormView
from django.views.generic import ListView


# Call of models:

from .models import GeoPoliticalLevelName
from .models import Country
from .models import FirstLevelName
from .models import SecondLevelName
from .models import ThirdLevelName
from .models import FirstLevel
from .models import SecondLevel
from .models import ThirdLevel
from .models import Location

# call for forms

# GeoPoliticalLevelName
# The names for different geopoliticallevels
from django.utils.translation import ugettext as _

class Saved(View):
	template_name="saved.html"

# CRUD

class GPNC_CreateView(CreateView, LoginRequiredMixin):
	login_url = '/login/'
	redirect_field_name = "GPN_create_view"

	model=GeoPoliticalLevelName
	template_name="localization/gpnc_create_form.html"
	fields=['name','language']
	success_url = "/success/"

