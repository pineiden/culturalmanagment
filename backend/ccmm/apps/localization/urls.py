from django.conf.urls import include, url

from .views import Saved
from .views import GPNC_CreateView

from django.utils.translation import ugettext as _
from django.conf.urls.i18n import i18n_patterns

urlpatterns=[
	url(_(r'success'),Saved.as_view(), name="success"),
	url(_(r'create/gpnc'), GPNC_CreateView.as_view(), name='GPNC_create')

]
