import os
import csv
from apps.localization.models import GeoPoliticalLevelName, Country,\
    FirstLevelName, SecondLevelName, ThirdLevelName, FirstLevel,\
    SecondLevel, ThirdLevel, Location

# LOAD GeoPoliticalLevelName
file="geonames.csv"
with open(file) as csvfile:
	reader=csv.DictReader(csvfile, delimiter=';')
	for row in reader:
		print(row)