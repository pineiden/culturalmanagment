awk -F';' '
	function ltrim(s) { 
		sub(/^[ \t\r\n]+/, "", s); return s }
	function rtrim(s) { 
		sub(/[ \t\r\n]+$/, "", s); return s }
	function trim(s) { 
		return rtrim(ltrim(s)); }
	(NR>1){
	OFS=";";
	cod_prov=$4;
	cod_region=substr($4,1,2); 
	cod_p=substr($4,3,length($4));
	print (trim($3),cod_region, cod_p)}' chile_rpc.csv | sed 's/\xc2\xa0//g'|sort|uniq>provincias.csv

awk -F';' '{OFS=";";print NR, $0,1}' provincias.csv >>data/second_level.csv

