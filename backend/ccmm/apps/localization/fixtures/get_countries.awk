awk -F';' '
	function ltrim(s) { sub(/^[ \t\r\n]+/, "", s); return s }
	function rtrim(s) { sub(/[ \t\r\n]+$/, "", s); return s }
	function trim(s) { return rtrim(ltrim(s)); }
	{
	OFS=";";
	if ($2 ~ /AFRICA/){
		continent="af"}
	else if($2 ~ /OCEANIA/){
		continent="oc"}
	else if($2 ~ /BALTICS/){
		continent="eu"} 		 
	else if($2 ~ /ASIA/){
		continent="as"} 
	else if($2 ~ /EUROPE/){
		continent="eu"} 
	else if($2~/AMER/){
		continent="am"} 
	else if($2~/NEAR EAST/){
		continent="as"}
	else if($2~/C.W./){
		continent="eu"}
	else{continent=$2};
	print (NR-2,rtrim($1),continent)}' countries_all.csv 

