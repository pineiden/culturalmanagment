from django.core.exceptions import ValidationError

def file_size(value, size): # add this to some file where you can import it from
	"""
	Validate the file size, given the file object and size limit.
	"""
	if value.size > size:
		raise ValidationError('File too large. Size should not exceed %s bytes' % size)
