# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-03 02:15
from __future__ import unicode_literals

import apps.localization.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('continent', models.CharField(choices=[('am', 'America'), ('eu', 'Europe'), ('af', 'Africa'), ('as', 'Asia'), ('oc', 'Oceania')], max_length=2, verbose_name='continent')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
                ('flag', models.FileField(upload_to='localization/countries_flags', validators=[apps.localization.models.flag_size], verbose_name='Flag')),
            ],
        ),
        migrations.CreateModel(
            name='FirstLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
                ('code', models.CharField(default='', max_length=4, unique=True, verbose_name='Number')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.Country', verbose_name="First's Level Country")),
            ],
        ),
        migrations.CreateModel(
            name='FirstLevelName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.Country')),
            ],
        ),
        migrations.CreateModel(
            name='GeoPoliticalLevelName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=50, verbose_name='Street')),
                ('number', models.IntegerField()),
                ('geopoint', django.contrib.gis.db.models.fields.PointField(srid=4326)),
            ],
            options={
                'verbose_name': 'Location',
                'default_related_name': 'location',
            },
        ),
        migrations.CreateModel(
            name='SecondLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
                ('code', models.CharField(default='', max_length=6, unique=True, verbose_name='Number')),
            ],
        ),
        migrations.CreateModel(
            name='SecondLevelName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.Country')),
                ('level_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.GeoPoliticalLevelName')),
            ],
        ),
        migrations.CreateModel(
            name='ThirdLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Name')),
                ('code', models.CharField(default='', max_length=10, unique=True, verbose_name='Number')),
                ('url', models.URLField(verbose_name='URL Address')),
            ],
        ),
        migrations.CreateModel(
            name='ThirdLevelName',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.Country')),
                ('level_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.GeoPoliticalLevelName')),
            ],
        ),
        migrations.AddField(
            model_name='thirdlevel',
            name='geo_political_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.ThirdLevelName'),
        ),
        migrations.AddField(
            model_name='thirdlevel',
            name='parent_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.SecondLevel', verbose_name="Third's Level Second Level"),
        ),
        migrations.AlterOrderWithRespectTo(
            name='thirdlevel',
            order_with_respect_to='code',
        ),
        migrations.AddField(
            model_name='secondlevel',
            name='geo_political_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.SecondLevelName'),
        ),
        migrations.AddField(
            model_name='secondlevel',
            name='parent_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.FirstLevel', verbose_name="Second's Level First Level"),
        ),
        migrations.AlterOrderWithRespectTo(
            name='secondlevel',
            order_with_respect_to='code',
        ),
        migrations.AddField(
            model_name='location',
            name='third_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='location', to='localization.ThirdLevel', verbose_name="Location's Third Level"),
        ),
        migrations.AlterOrderWithRespectTo(
            name='location',
            order_with_respect_to='street',
        ),
        migrations.AddField(
            model_name='firstlevelname',
            name='level_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.GeoPoliticalLevelName'),
        ),
        migrations.AddField(
            model_name='firstlevel',
            name='geo_political_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localization.FirstLevelName'),
        ),
        migrations.AlterOrderWithRespectTo(
            name='firstlevel',
            order_with_respect_to='code',
        ),
        migrations.AddField(
            model_name='country',
            name='first_level_name',
            field=models.ManyToManyField(related_name='country_first_level_name', through='localization.FirstLevelName', to='localization.GeoPoliticalLevelName'),
        ),
        migrations.AddField(
            model_name='country',
            name='second_level_name',
            field=models.ManyToManyField(related_name='country_second_level_name', through='localization.SecondLevelName', to='localization.GeoPoliticalLevelName'),
        ),
        migrations.AddField(
            model_name='country',
            name='third_level_name',
            field=models.ManyToManyField(related_name='country_third_level_name', through='localization.ThirdLevelName', to='localization.GeoPoliticalLevelName'),
        ),
        migrations.AddIndex(
            model_name='thirdlevel',
            index=models.Index(fields=['code', 'name'], name='localizatio_code_3e9d18_idx'),
        ),
        migrations.AddIndex(
            model_name='secondlevel',
            index=models.Index(fields=['code', 'name'], name='localizatio_code_bad4c1_idx'),
        ),
        migrations.AddIndex(
            model_name='location',
            index=models.Index(fields=['street', 'number', 'third_level'], name='localizatio_street_36558d_idx'),
        ),
        migrations.AddIndex(
            model_name='firstlevel',
            index=models.Index(fields=['code', 'name'], name='localizatio_code_94b3a7_idx'),
        ),
    ]
