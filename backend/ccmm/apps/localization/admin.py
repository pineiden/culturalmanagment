from django.contrib import admin
from django.contrib.gis.db import models as gis_models
from django.contrib.gis import admin as gis_admin


from apps.localization.models import GeoPoliticalLevelName, Country,\
    FirstLevelName, SecondLevelName, ThirdLevelName, FirstLevel,\
    SecondLevel, ThirdLevel, Location

# Register your models here.

# 1° GeoPoliticalLevelName

class GeoPoliticalLevelNameAdmin(admin.ModelAdmin):
    pass
admin.site.register(GeoPoliticalLevelName, GeoPoliticalLevelNameAdmin)

# Country
class CountryAdmin(admin.ModelAdmin):
	pass
admin.site.register(Country, CountryAdmin)

# Level Names:
class FirstLevelNameAdmin(admin.ModelAdmin):
	pass
admin.site.register(FirstLevelName, FirstLevelNameAdmin)

class SecondLevelNameAdmin(admin.ModelAdmin):
	pass
admin.site.register(SecondLevelName, SecondLevelNameAdmin)

class ThirdLevelNameAdmin(admin.ModelAdmin):
	pass
admin.site.register(ThirdLevelName, ThirdLevelNameAdmin)

# Levels

class FirstLevelAdmin(admin.ModelAdmin):
	pass
admin.site.register(FirstLevel, FirstLevelAdmin)

class SecondLevelAdmin(admin.ModelAdmin):
	pass
admin.site.register(SecondLevel, SecondLevelAdmin)

class ThirdLevelAdmin(admin.ModelAdmin):
	pass
admin.site.register(ThirdLevel, ThirdLevelAdmin)


# Location

class LocationAdmin(gis_admin.GeoModelAdmin):
	map_width=1000
	map_heigth=800
	default_lon=-70
	default_lat=-33
	default_zoom=10
	extra_js=''
admin.site.register(Location, LocationAdmin)

