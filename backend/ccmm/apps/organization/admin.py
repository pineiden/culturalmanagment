from django.contrib import admin

# Register your models here.
from .models import KindOfOrganization, Organization, OrganizationAlbum, OrganizationPlace

admin.site.register(KindOfOrganization)
admin.site.register(Organization)
admin.site.register(OrganizationAlbum)
admin.site.register(OrganizationPlace)