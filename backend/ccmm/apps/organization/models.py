from django.db import models

#import validators
from validators.mimetype import MimetypeValidator
from validators.size import FileSize
# Create your models here.
from django.utils.translation import ugettext as _

# import from gallery
from apps.gallery.models import Album

# import from localization
from apps.place.models import Place

class KindOfOrganization(models.Model):
	"""
	There are different kinds of organization, so clasify in
	relation to what it do
	"""
	name=models.CharField(max_length=30, 
		verbose_name=_("Name"))
	description=models.TextField(
		verbose_name=_("Description"))
	symbol=models.FileField(
		upload_to="symbol",
		validators=[
		MimetypeValidator('image/svg+xml'),
		FileSize(
			lower_limit=0,
			higher_limit=1024000)],
		verbose_name=_("Symbol"))
	url=models.URLField(max_length=300,
		verbose_name=_("URL Address"))
	class Meta:
		verbose_name=_("Kind of Organization")
		verbose_name_plural=_("Kinds of Organizations")


class Organization(models.Model):
	"""
	Is an organization register to contain the information about that
	"""
	name=models.CharField(max_length=60,
		verbose_name=_("Name"))
	description=models.TextField(verbose_name=_("Description"))
	url=models.URLField(max_length=300,verbose_name=_("URL Address"))
	wiki=models.URLField(max_length=300,
		verbose_name=_("Wiki URL"))
	symbol=models.FileField(
		upload_to="symbol",
		validators=[
		MimetypeValidator(
		[
		'image/svg+xml',
		'image/png',
		'image/jpeg'
		]),
		FileSize(
			lower_limit=0,
			higher_limit=1024000)],
		verbose_name=_("Symbol"))
	kind=models.ForeignKey(KindOfOrganization,
		verbose_name=_("Kind"))

	class Meta:
		verbose_name=_("Organization")
		verbose_name_plural=_("Organizations")

class OrganizationAlbum(models.Model):
	"""
	An album related to some organizacion
	"""
	CHOICE_PRIORITY=[(i,i-25) for i in range(51)]# send hardcoded value to settings
	organization=models.ForeignKey(
		Organization,
		verbose_name=_("Organization"))
	album=models.ForeignKey(Album,
		verbose_name=_("Album"))
	principal=models.BooleanField(
		default=False,
		verbose_name=_("Principal"))
	position=models.IntegerField(
		choices=CHOICE_PRIORITY,
		verbose_name=_("Position"))

	class Meta:
		verbose_name=_("Organization's Album")
		verbose_name_plural=_("Organizations's Albums")

class OrganizationPlace(models.Model):
	"""
	The places in what the organizacion works
	"""
	CHOICE_PRIORITY=[(i,i-25) for i in range(51)]# send hardcoded value to settings
	organization=models.ForeignKey(
		Organization,
		verbose_name=_("Organization"))
	place=models.ForeignKey(
		Place,
		verbose_name=_("Place"))
	principal=models.BooleanField(
		default=False,
		verbose_name=_("Principal"))
	position=models.IntegerField(
		choices=CHOICE_PRIORITY,
		verbose_name=_("Positon"))
	class Meta:
		verbose_name=_("Organization Place")
		verbose_name_plural=_("Organization's Places")	