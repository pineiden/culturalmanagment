from django.db import models

# Create your models here.
from django.utils.translation import ugettext_lazy as _

# Import from apps

from apps.team.models import Team
from apps.gallery.models import Album
from apps.funds.models import Fund
from apps.ccmm_profile.models import CultureManagmentUser
from apps.organization.models import Organization

#import validators
from validators.mimetype import MimetypeValidator
from validators.size import FileSize

# datetime
from datetime import datetime, timedelta

# Slug
from autoslug import AutoSlugField

def get_upload_project_document(instance, filename):
    return "project/%s/document/%s" %(instance.project.slug, instance.slug)

def end_date():
	return datetime.today()+timedelta(days=30)

class Project(models.Model):
	"""
	Information about a project
	"""
	name=models.CharField(
		max_length=200,
		verbose_name=_("Name"))
	slug = AutoSlugField(blank=True,
		populate_from='name', 
		editable=True,
		verbose_name=_("Project's Slug"))
	symbol=models.FileField(
		upload_to="symbol",
		validators=[
		MimetypeValidator('image/svg+xml'),
		FileSize(
			lower_limit=0,
			higher_limit=1024000)],
		verbose_name=_("Symbol"))
	description=models.TextField(
		verbose_name=_("Description"))
	date_start=models.DateField(default=datetime.today,
		verbose_name=_("Start Date"))
	date_end=models.DateField(default=end_date,
		verbose_name=_("End Date"))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project")
		verbose_name_plural=_("Projects")

class OrganizationSupportProject(models.Model):
	"""
	Organization that supports the project
	"""
	SUPPORT_TYPE=[
	('spc',_("Space")),
	('ptr',_("Sponsorship")),
	('mda',_("Media")),
	('fnd',_("Funds")),
	('alc',_("Alliance"))
	]
	organization=models.ForeignKey(Organization,
		on_delete=models.CASCADE,
		related_name="project_supports",#plural
		related_query_name="project_support",		
		)	
	support=models.CharField(max_length=3, 
		choices=SUPPORT_TYPE,
		verbose_name=_("Support"))
	project=models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="project_supports",#plural
		related_query_name="project_support",		
		)

	def __str__(self):
		return "%s -> %s" %(
			self.organization, 
			self.project)

	class Meta:
		verbose_name=_("Project")
		verbose_name_plural=_("Projects")


class PauseProject(models.Model):
	"""
	A pause in the development of the project
	"""
	project=models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="project_pauses",#plural
		related_query_name="project_pause",		
		)
	date_start=models.DateField(default=datetime.today,
		verbose_name=_("Start Date"))
	date_end=models.DateField(default=end_date,
		verbose_name=_("End Date"))
	commentary=models.TextField(verbose_name=_("Commentary"))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project's Pause")
		verbose_name_plural=_("Project's Pauses")


class ProjectFund(models.Model):
	"""
	Fund assigned to the project
	"""
	project = models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="project_funds",#plural
		related_query_name="project_fund",		
		)
	fund = models.ForeignKey(Fund,
		on_delete=models.CASCADE,
		related_name="project_funds",#plural
		related_query_name="project_fund",		
		)
	
	def __str__(self):
		return "%s para %s" %(self.fund, self.project)

	class Meta:
		verbose_name=_("Project's Fund")
		verbose_name_plural=_("Project's Funds")


class ProjectTeam(models.Model):
	"""
	Team assigned to the project
	"""
	project  = models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="project_teams",#plural
		related_query_name="project_team",		
		)
	team  = models.ForeignKey(Team,
		on_delete=models.CASCADE,
		related_name="project_teams",#plural
		related_query_name="project_team",		
		)

	def __str__(self):
		return "%s de %s" % (self.team, self.project)

	class Meta:
		verbose_name=_("Project's Team")
		verbose_name_plural=_("Project's Teams")


class ProjectStage(models.Model):
	"""
	Define a stage related to the project
	"""
	project = models.ForeignKey(Project,
		on_delete=models.CASCADE,
		related_name="projects_stage",#plural
		related_query_name="project_stage",		
		)
	name = models.CharField(max_length=200)
	description=models.TextField(verbose_name=_("Description"))
	
	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project's Stage")
		verbose_name_plural=_("Project's Stages")

class ProjectTask(models.Model):
	"""
	Define a task and assign to a team or team member
	"""
	CHOICES_IMPORTANCE=[
		(0,_("Very Low")),
		(2,_("Low")),
		(3,_("Medium")),
		(4,_("High")),
		(5,_("Very High"))
	]
	stage = models.ForeignKey(ProjectStage,
		on_delete=models.CASCADE,
		related_name="projects_task",#plural
		related_query_name="project_task",		
		)
	date_start=models.DateField(default=datetime.today,
		verbose_name=_("Start Date"))
	date_end=models.DateField(default=end_date,
		verbose_name=_("End Date"))
	goal= models.TextField(verbose_name=_("Goals"))
	responsable= models.ForeignKey(CultureManagmentUser,
		on_delete=models.CASCADE,
		related_name="projects_task",#plural
		related_query_name="project_task",		
		)
	importance=models.IntegerField(choices=CHOICES_IMPORTANCE,
		verbose_name=_("Task's Importance"))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project Task")
		verbose_name_plural=_("Project Tasks")

class ProjectTaskDocument(models.Model):
	"""
	Create and upload documents related to the Task of the 
	Project
	"""
	task=models.ForeignKey(ProjectTask,
		on_delete=models.CASCADE,
		related_name="projects_task_document",#plural
		related_query_name="project_task_document",		
		) 
	document=models.FileField(
		upload_to=get_upload_project_document,
		validators=[
		MimetypeValidator('image/svg+xml'),
		FileSize(
			lower_limit=0,
			higher_limit=6*1024000)],
		verbose_name=_("Symbol"))	
	name = models.CharField(
		max_length=100,
		verbose_name=_('Name'))
	slug = AutoSlugField(blank=True,
		populate_from='name', 
		editable=True,
		verbose_name=_("Project's Document Slug"))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project Task's Document")
		verbose_name_plural=_("Project Task's Documents")


class ProjectTaskMedia(models.Model):
	"""
	Create the related information to media resources
	that registered the development of some task
	"""
	task = models.ForeignKey(ProjectTask,
		on_delete=models.CASCADE,
		related_name="projects_task_media",#plural
		related_query_name="project_task_media",		
		)
	url = models.URLField(max_length=300)
	name=models.CharField(
		max_length=100,
		verbose_name=_('Name'))
	description=models.TextField(verbose_name=_("Description"))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name=_("Project Task's Media")
		verbose_name_plural=_("Project Task's Medias")


class ProjectTaskAlbum(models.Model):
	"""
	A set of pictures related to the Task
	"""
	task = models.ForeignKey(ProjectTask, 
		on_delete=models.CASCADE,
		related_name="projects_task_album",#plural
		related_query_name="project_task_album",
		null=True			
		)
	album = models.ForeignKey(Album, 
		on_delete=models.CASCADE,
		related_name="projects_task_album",#plural
		related_query_name="project_task_album",	
		null=True	
		)

	def __str__(self):
		return "Albúm %s de %s" %(self.album,self.task)

	class Meta:
		verbose_name=_("Project Task Album")
		verbose_name_plural=_("Project Task's Albums")
