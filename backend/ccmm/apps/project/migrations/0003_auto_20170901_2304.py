# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-01 23:04
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_auto_20170901_2304'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='date_start',
            field=models.DateField(default=datetime.datetime.today, verbose_name='Start Date'),
        ),
    ]
