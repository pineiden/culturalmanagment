from django.contrib import admin
from .models import (Project,
	OrganizationSupportProject,
	PauseProject,
	ProjectFund,
	ProjectTeam,
	ProjectStage,
	ProjectTask,
	ProjectTaskDocument,
	ProjectTaskMedia,
	ProjectTaskAlbum)
	
# Register your models here.

admin.site.register(Project)
admin.site.register(OrganizationSupportProject)
admin.site.register(PauseProject)
admin.site.register(ProjectFund)
admin.site.register(ProjectTeam)
admin.site.register(ProjectStage)
admin.site.register(ProjectTask)
admin.site.register(ProjectTaskDocument)
admin.site.register(ProjectTaskAlbum)
