from django.core.exceptions import ValidationError
import magic
from django.utils.translation import ugettext_lazy as _
from django.utils.deconstruct import deconstructible

@deconstructible
class MimetypeValidator(object):
	def __init__(self, mimetypes):
		self.mimetypes = mimetypes
	
	def __call__(self, value):
		try:
			mime = magic.from_buffer(value.read(1024), mime=True)
			if not mime in self.mimetypes:
				raise ValidationError(_('%s is not an acceptable file type' % value))
		except AttributeError as e:
			raise ValidationError(_('This value could not be validated for file type' % value))

	def __eq__(self, other):		
		mime = magic.from_buffer(other.read(1024), mime=True)
		assert(mime in mimetypes, _("This value it's not in the list of mimetypes"))